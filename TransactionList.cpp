//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Team: (indicate member names, students numbers and courses)

#include "TransactionList.h"

//---------------------------------------------------------------------------
//TransactionList: class implementation
//---------------------------------------------------------------------------

//____public member functions

//____constructors & destructors

//____other public member functions

void TransactionList::addNewTransaction(const Transaction& tr) {
    listOfTransactions_.push_front(tr);
}
const Transaction TransactionList::newestTransaction() const {
    return (listOfTransactions_.front());
}
const TransactionList TransactionList::olderTransactions() const {
	TransactionList trlist(*this);
    trlist.deleteFirstTransaction();
    return trlist;
}
void TransactionList::deleteFirstTransaction() {
    listOfTransactions_.pop_front();
}
void TransactionList::deleteGivenTransaction(const Transaction& tr) {
    listOfTransactions_.remove(tr);
}
void TransactionList::deleteTransactionsUpToDate(const Date& d) {
	
	if (size() != 0) {
		if (newestTransaction().getDate() <= d) {
			deleteFirstTransaction();
			deleteTransactionsUpToDate(d);
		}
		else {
			Transaction tr = newestTransaction();
			deleteFirstTransaction();
			deleteTransactionsUpToDate(d);
			addNewTransaction(tr);
		}
	}

}
int TransactionList::size() const {
    return (listOfTransactions_.size());
}

TransactionList TransactionList::getAllDepositTransactions() const
{
	TransactionList trl;

	list<Transaction> deposits;

	list<Transaction> copy(listOfTransactions_);
	while (copy.size() != 0) {
		if (copy.front().getAmount() > 0) {
			deposits.push_front(copy.front());
		}
		copy.pop_front();
	}

	trl.listOfTransactions_ = deposits;

	return trl;
}

TransactionList TransactionList::getTransactionsUpToDate(const Date& d) const {
	TransactionList copy = (*this);

	if (copy.size() != 0) {
		if (d < newestTransaction().getDate()) {
			return copy.olderTransactions().getTransactionsUpToDate(d);
			copy.deleteFirstTransaction();
		}
		else {
			Transaction tr(copy.newestTransaction());
			copy = copy.olderTransactions().getTransactionsUpToDate(d);
			copy.addNewTransaction(tr);
			return copy;
		}
	}
	else
		return copy;
}

TransactionList TransactionList::getMostRecentTransactions(int amount) const
{
	list<Transaction> copy = listOfTransactions_;
	TransactionList recentTransactions;
	for (int i = 0; i < amount; i++)
	{
		if (copy.size() != 0) {
			recentTransactions.addNewTransaction(copy.front());
			copy.pop_front();
		}
		else {
			break;
		}
	}
	return recentTransactions;
}

TransactionList TransactionList::getTransactionsForAmount(double amount) const
{
	list<Transaction> copy = listOfTransactions_;
	TransactionList toReturn;
	for (int i = 0; i < listOfTransactions_.size(); i++)
	{
		if (copy.front().getAmount() == amount) {
			toReturn.addNewTransaction(copy.front());
		}
		copy.pop_front();
	}

	
	return toReturn;
}

TransactionList TransactionList::getTransactionsForTitle(string title) const
{
	list<Transaction> copy = listOfTransactions_;
	TransactionList toReturn;
	for (int i = 0; i < listOfTransactions_.size(); i++)
	{
		if (copy.front().getTitle() == title) {
			toReturn.addNewTransaction(copy.front());
		}
		copy.pop_front();
	}


	return toReturn;
}

TransactionList TransactionList::getTransactionsForDate(Date date) const
{
	list<Transaction> copy = listOfTransactions_;
	TransactionList toReturn;
	for (int i = 0; i < listOfTransactions_.size(); i++)
	{
		if (copy.front().getDate() == date) {
			toReturn.addNewTransaction(copy.front());
		}
		copy.pop_front();
	}


	return toReturn;
}

double TransactionList::getTotalTransactions() const
{
	double total(0);
	list<Transaction> copy = listOfTransactions_;
	while (copy.size() != 0) {
		total += copy.front().getAmount();
		copy.pop_front();
	}
	return total;
}

const string TransactionList::toFormattedString() const {
//return transaction list as a (formatted) string
	ostringstream os_transactionlist;
    TransactionList tempTrList(*this);
	while (! (tempTrList.size() == 0))
    {
		os_transactionlist << endl << tempTrList.newestTransaction().toFormattedString();
        tempTrList.deleteFirstTransaction();
    }
	return (os_transactionlist.str());
}

ostream& TransactionList::putDataInStream(ostream& os) const {
//put (unformatted) transaction list into an output stream
    TransactionList tempTrList(*this);
	while (! (tempTrList.size() == 0))
    {
		os << tempTrList.newestTransaction() << endl;
        tempTrList.deleteFirstTransaction();
    }
	return os;
}
istream& TransactionList::getDataFromStream(istream& is) {
//read in (unformatted) transaction list from input stream
	Transaction aTransaction;
	is >> aTransaction;	//read first transaction
	while (is) 	//while not end of file
	{
		listOfTransactions_.push_back(aTransaction);   //add transaction to list of transactions
		is >> aTransaction;	//read in next transaction
	}
	return is;
}

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const TransactionList& aTransactionList) {
    return (aTransactionList.putDataInStream(os));
}
istream& operator>>(istream& is, TransactionList& aTransactionList) {
	return (aTransactionList.getDataFromStream(is));
}
