#ifndef ChildAccountH
#define ChildAccountH

#include "SavingsAccount.h"

#include <fstream>
#include <cassert>
using namespace std;

class ChildAccount : public SavingsAccount 
{
public:
	ChildAccount();
	virtual ~ChildAccount();

	double getMaximumPaidIn() const;
	double getMinimumPaidIn() const;

	virtual double maxBorrowable() const;
	virtual bool canDeposit(double amount) const;

	virtual const string prepareFormattedAccountDetails() const;

	virtual istream& getAccountDataFromStream(istream& is);
	virtual ostream& putAccountDetailsInStream(ostream& os) const;


private:
	double maximumPaidIn_;
	double minimumPaidIn_;

};


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const ChildAccount&);	//output operator
istream& operator>>(istream&, ChildAccount&);	    //input operator



#endif 

