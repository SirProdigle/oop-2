#ifndef CurrentAccounth
#define CurrenAccountH

#include "BankAccount.h"

#include <fstream>
#include <cassert>
using namespace std;

class CurrentAccount : public BankAccount
{
public:
	CurrentAccount();
	virtual ~CurrentAccount();

	double getOverDraftLimit() const;
	virtual double maxBorrowable() const;
	
	virtual const string prepareFormattedAccountDetails() const;
	
	virtual istream& getAccountDataFromStream(istream& is);
	virtual ostream& putAccountDetailsInStream(ostream& os) const;

private:
	double overdraftLimit_;

	
};

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const CurrentAccount&);	//output operator
istream& operator>>(istream&, CurrentAccount&);	    //input operator

#endif 

