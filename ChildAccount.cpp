#include "ChildAccount.h"
#include "Constants.h"

//---------------------------------------------------------------------------
//ChildAccount: class implementation
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------



//____constructors & destructors

ChildAccount::ChildAccount() : minimumPaidIn_(0.0), maximumPaidIn_(0.0), SavingsAccount() {};
ChildAccount::~ChildAccount() {};

//____other public functions

double ChildAccount::getMinimumPaidIn() const {
	return minimumPaidIn_;
}

double ChildAccount::getMaximumPaidIn() const {
	return maximumPaidIn_;
}

double ChildAccount::maxBorrowable() const{
	return 0;
}

bool ChildAccount::canDeposit(double amount) const {
	return (amount >= minimumPaidIn_ && amount <= maximumPaidIn_);
}

istream& ChildAccount::getAccountDataFromStream(istream& is) {
	//get BankAccount details from stream
	SavingsAccount::getAccountDataFromStream(is);
	is >> minimumPaidIn_;		//get minimum paid in requirement
	is >> maximumPaidIn_;		//get maximum paid in requirement
	return is;
}

ostream& ChildAccount::putAccountDetailsInStream(ostream& os) const {
	//put (unformatted) BankAccount details in stream
	SavingsAccount::putAccountDetailsInStream(os);
	os << minimumPaidIn_ << "\n";
	os << maximumPaidIn_ << "\n";
	return os;
}

const string ChildAccount::prepareFormattedAccountDetails() const
{
	assert(getAccountType(getAccountNumber()[0]) != "UNKNOWN");
	ostringstream os;
	os << SavingsAccount::prepareFormattedAccountDetails();
	os << "\n      MINIMUM PAYMENT REQUIRED:\234" << setw(10) << minimumPaidIn_;
	os << "\n      MAXIMUM PAYMENT REQUIRED:\234" << setw(10) << maximumPaidIn_;
	os << "\n      ----------------------------------------";
	return os.str();
}


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const ChildAccount& aBankAccount) {
	//put (unformatted) BankAccount details in stream
	return aBankAccount.putDataInStream(os);
}
istream& operator>>(istream& is, ChildAccount& aBankAccount) {
	//get BankAccount details from stream
	return aBankAccount.getDataFromStream(is);
}