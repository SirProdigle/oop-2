//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Team: (indicate member names, students numbers and courses)

#ifndef ATMH
#define ATMH 

//---------------------------------------------------------------------------
//ATM: class declaration
//---------------------------------------------------------------------------

#include "Card.h"
#include "BankAccount.h"
#include "CurrentAccount.h"
#include "SavingsAccount.h"
#include "ChildAccount.h"
#include "ISAAccount.h"
#include "UserInterface.h"
#include "Date.h"
#include "Time.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
using namespace std;

class ATM {
public:
	ATM();	//default constructor
	~ATM();	//destructor
    void activateCashPoint();
private:
	//data items
    BankAccount* p_theActiveAccount_;
    Card* p_theCard_;
    UserInterface* theUI_;

	//support functions
    int  validateCard(const string&) const;
	void executeCardCommand(int);
	int  validateAccount(const string&) const;
	void executeAccountCommand();
	void attemptTransfer(BankAccount*);


	//card menu commands
	void m_card1_manageIndividualAccount();
	void m_card1_showFundsAvailableOnAllAccounts();
	//account menu commands
    void m_acct1_produceBalance() const;
    void m_acct2_withdrawFromBankAccount();
    void m_acct3_depositToBankAccount();
    void m_acct4_produceStatement() const;
	void m_acct5_showAllDepositsTransactions() const;
	void m_acct6_showMiniStatement() const;
	void m_acct7_searchForTransactions() const;
	void m_acct8_clearTransactionUpToDate() const;
	void m_acct9_transferCashToAnotherAccount();
	void searchTransactions() const;

	void m_trl1_showTransactionsForAmount() const;
	void m_trl1_showTransactionsForTitle() const;
	void m_trl1_showTransactionsForDate() const;

    //support file handling functions and creation of dynamic objects
    bool canOpenFile(const string&) const;
	static char getAccountTypeCode(const string&);
	bool accountsListedOnCard(const string&) const;
    void activateCard(const string&);
	void releaseCard();
	void recordTransfer(double, BankAccount*);
	BankAccount* activateAccount(const string&);
	BankAccount* releaseAccount(BankAccount*, string);
};

#endif
