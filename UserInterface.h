//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Team: (indicate member names, students numbers and courses)


#ifndef UserInterfaceH 
#define UserInterfaceH

//---------------------------------------------------------------------------
//UserInterface: class declaration
//---------------------------------------------------------------------------

#include "constants.h"
#include "Time.h"
#include "Date.h"
#include "MiniStatement.h"
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

class UserInterface {
public:

	//Singleton Implementation
	static UserInterface* getInstance() {
		static UserInterface instance;
		return &instance;
	}


	void showByeScreen() const;

	int showMainMenuAndGetCommand() const;
	int showCardMenuAndGetCommand(const string& cardNum) const;
	int showAccountMenuAndGetCommand(const string& accNum) const;

	void showErrorInvalidCommand() const;
	void wait() const;
	void endProgram() const;
	const string readInCardToBeProcessed() const;
	void showValidateCardOnScreen(int validCode, const string& cardNum) const;
	void showCardAccounts(const string& cardNum, const string& cardSt) const;
	const string readInAccountToBeProcessed() const;
	void showValidateAccountOnScreen(int valid, const string& acctNum) const;
	void showCardOnScreen(string card);

	static const string cardFilename(const string& cn);
	static const string accountFilename(const string& an);

	double readInWithdrawalAmount() const;
	double readInDepositAmount() const;
	double readInTransferAmount() const;
	Date readInValidDate(Date&) const;
	bool readInConfirmDelete() const;
	int readInNumberOfTransactions() const;
	int readInPositiveNumber() const;
	string readInTitle() const;

	void showProduceBalanceOnScreen(double bal) const;
	void showDepositOnScreen(bool auth, double deposit) const;
	void showWithdrawalOnScreen(bool auth, double withdrawal) const;
	void showStatementOnScreen(const string&) const;
	void showMiniStatementOnScreen(bool, double total , std::string statement ) const;
	void showAllDepositsOnScreen(bool noTransaction, string& str, double& total) const;
	void showTransactionsUpToDate(bool noTrans, Date& d, string& str, int& n) const;
	void showDeletionOfTransactionsUpToDate(int n, Date& , bool) const;
	void showSearchMenu() const;
	int readInSearchCommand() const;
	void showNoTransactionsOnScreen() const;
	double readInDouble() const;
	void showMatchingTransactionsOnScreen(double, int, string) const; 
	void showMatchingTransactionsOnScreen(string, int, string) const;
	void showMatchingTransactionsOnScreen(Date, int, string) const;
	void showFundsAvailableOnScreen(double, string)const;
	void showErrorMessage(string) const;

	void showTransferOnScreen(bool outOk, bool inOk, double transferAmount)const;
private:
	//support functions
	int readInCommand() const;
	double readInPositiveAmount() const;
	void outputHeader(const string&) const;
	string askForInput(const string&) const;
	void outputLine(const string&) const;

	//Singleton Implementation
	UserInterface();
	UserInterface(const UserInterface&);
	UserInterface& operator=(const UserInterface&);
};

#endif
