#include "SavingsAccount.h"
#include "Constants.h"

//---------------------------------------------------------------------------
//SavingsAccount: class implementation
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------



//____constructors & destructors

SavingsAccount::SavingsAccount() : BankAccount(), minimumBalance_(0.0) {};

SavingsAccount::~SavingsAccount() {};

//____other public functions

double SavingsAccount::getMinimumBalance() const {
	return minimumBalance_;
}

double SavingsAccount::maxBorrowable() const {
	return getBalance() - minimumBalance_;
}

istream& SavingsAccount::getAccountDataFromStream(istream& is) {
	//get BankAccount details from stream
	BankAccount::getAccountDataFromStream(is);
	is >> minimumBalance_;		//get minimum balance
	return is;
}

ostream& SavingsAccount::putAccountDetailsInStream(ostream& os) const {
	//put (unformatted) BankAccount details in stream
	BankAccount::putAccountDetailsInStream(os);
	os << minimumBalance_ << "\n";
	return os;
}

const string SavingsAccount::prepareFormattedAccountDetails() const
{
	assert(getAccountType(getAccountNumber()[0]) != "UNKNOWN");
	ostringstream os;
	//os << BankAccount::prepareFormattedAccountDetails();

	os << "\n      ACCOUNT TYPE:    " << getAccountType(getAccountNumber()[0]) << " ACCOUNT";
	os << "\n      ACCOUNT NUMBER:  " << getAccountNumber();
	os << "\n      CREATION DATE:   " << getCreationDate().toFormattedString();
	os << fixed << setprecision(2) << setfill(' ');
	os << "\n      BALANCE:         \234" << setw(10) << getBalance();
	os << "\n      ----------------------------------------";
	return os.str();
	os << "\n      MINIMUM BALANCE:       \234" << setw(10) << minimumBalance_;
	os << "\n      ----------------------------------------";
	return os.str();
}


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const SavingsAccount& aBankAccount) {
	//put (unformatted) BankAccount details in stream
	return aBankAccount.putDataInStream(os);
}
istream& operator>>(istream& is, SavingsAccount& aBankAccount) {
	//get BankAccount details from stream
	return aBankAccount.getDataFromStream(is);
}