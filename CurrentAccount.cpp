#include "CurrentAccount.h"
#include "Constants.h"

//---------------------------------------------------------------------------
//BankAccount: class implementation
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------



//____constructors & destructors

CurrentAccount::CurrentAccount() : BankAccount(), overdraftLimit_(0.0) {};

CurrentAccount::~CurrentAccount() {};

//____other public functions

double CurrentAccount::getOverDraftLimit() const {
	return overdraftLimit_;
}

double CurrentAccount::maxBorrowable() const {
	//check if enough money in account
	return getBalance() + overdraftLimit_;
}

istream& CurrentAccount::getAccountDataFromStream(istream& is) {
	//get BankAccount details from stream
	BankAccount::getAccountDataFromStream(is);
	is >> overdraftLimit_;		//get overdraft
	return is;
}

ostream& CurrentAccount::putAccountDetailsInStream(ostream& os) const {
	//put (unformatted) BankAccount details in stream
	BankAccount::putAccountDetailsInStream(os);
	os << overdraftLimit_;
	return os;
}

const string CurrentAccount::prepareFormattedAccountDetails() const
{
	assert(getAccountType(getAccountNumber()[0]) != "UNKNOWN");
	ostringstream os;
	//os << BankAccount::prepareFormattedAccountDetails();

	os << "\n      ACCOUNT TYPE:    " << getAccountType(getAccountNumber()[0]) << " ACCOUNT";
	os << "\n      ACCOUNT NUMBER:  " << getAccountNumber();
	os << "\n      CREATION DATE:   " << getCreationDate().toFormattedString();
	os << fixed << setprecision(2) << setfill(' ');
	os << "\n      BALANCE:         \234" << setw(10) << getBalance();
	os << "\n      ----------------------------------------";
	return os.str();
	os << "\n      OVERDRAFT:       \234" << setw(10) << overdraftLimit_;
	os << "\n      ----------------------------------------";
	return os.str();
}


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const CurrentAccount& aBankAccount) {
	//put (unformatted) BankAccount details in stream
	return aBankAccount.putDataInStream(os);
}
istream& operator>>(istream& is, CurrentAccount& aBankAccount) {
	//get BankAccount details from stream
	return aBankAccount.getDataFromStream(is);
}