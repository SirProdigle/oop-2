//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Team: (indicate member names, students numbers and courses)

#ifndef BankAccountH
#define BankAccountH

//---------------------------------------------------------------------------
//BankAccount: class declaration
//---------------------------------------------------------------------------

//#include "Date.h"
//#include "Transaction.h"
#include "TransactionList.h"
#include "MiniStatement.h"
#include <fstream>
#include <cassert>
using namespace std;


class BankAccount {
public:
    //constructors & destructor
	BankAccount();
    virtual ~BankAccount();


	//getter (assessor) functions
    const string getAccountNumber() const;
    const Date getCreationDate() const;
	double getBalance() const;
    const TransactionList getTransactions() const;
	void produceTransactionsForAmount(double amount,string&,int&) const;
	void produceTransactionsForTitle(string title, int&, string&) const;
	void produceTransactionsForDate(Date date, int&, string&) const;

    bool	isEmptyTransactionList() const;
	void produceAllDepositTransactions(string&, double&) const;
	MiniStatement produceNMostRecentTransactions(int) const;
	string prepareFormattedMiniAccountDetails() const;

	//other operations
	const string prepareFormattedStatement() const;

    virtual void recordDeposit(double amount);
	void recordDeletionOfTransactionUpToDate(const Date&);

	virtual double maxBorrowable() const;
	bool canWithdraw(double amount) const;
	virtual bool canDeposit(double amount) const;
    void recordWithdrawal(double amount);
	void recordTransferOut(double amount, string AccNum);
	void recordTransferIn(double amount, string AccNum);
	bool canTransferOut(double amount) const;
	bool canTransferIn(double amount) const;

	void produceTransactionUpToDate(Date& d, string& str, int& size) const;

	void readInBankAccountFromFile(const string& fileName);
	void storeBankAccountInFile(const string& fileName) const;
	//functions to put data into and get data from streams
	ostream& putDataInStream(ostream& os) const;
	ostream& putAccountDetailsInStream(ostream& os) const;
	istream& getDataFromStream(istream& is);
	virtual istream& getAccountDataFromStream(istream& is);

	virtual const string prepareFormattedAccountDetails() const;
	const string prepareFormattedTransactionList() const;

	static const string getAccountType(const string& filename);
	static const string getAccountType(char n);
	
private:
    //data items
    string accountNumber_;
    Date   creationDate_;
	double balance_;
    TransactionList transactions_;
 
	//support functions
	void updateBalance(double amount);
	ostream& putTransactionInStream(ostream& os) const;
	istream& getTransactionsDataFromStream(istream& is);
};

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const BankAccount&);	//output operator
istream& operator>>(istream&, BankAccount&);	    //input operator

#endif
