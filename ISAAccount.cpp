#include "ISAAccount.h"
#include "Constants.h"

//---------------------------------------------------------------------------
//ISAAccount: class implementation
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------



//____constructors & destructors

ISAAccount::ISAAccount() : maximumYearlyDeposit_(0.0), currentYearlyDeposit_(0.0), SavingsAccount() {};
ISAAccount::~ISAAccount() {};

//____other public functions

double ISAAccount::getMaximumYearlyDeposit() const {
	return maximumYearlyDeposit_;
}

double ISAAccount::getCurrentYearlyDeposit() const {
	return currentYearlyDeposit_;
}

Date ISAAccount::getEndDepositPeriod() const {
	return endDepositPeriod_;
}

void ISAAccount::recordDeposit(double amountToDeposit) {
	updateCurrentYearlyDeposit(amountToDeposit);
	BankAccount::recordDeposit(amountToDeposit);
}

bool ISAAccount::canDeposit(double amount) const{
	return (((currentYearlyDeposit_ + amount) <= maximumYearlyDeposit_) && (endDepositPeriod_.currentDate() <= endDepositPeriod_));
}


istream& ISAAccount::getAccountDataFromStream(istream& is) {
	//get BankAccount details from stream
	SavingsAccount::getAccountDataFromStream(is);
	is >> maximumYearlyDeposit_;		//get maximum yearly deposit requirement
	is >> currentYearlyDeposit_;		//get current yearly deposit requirement
	is >> endDepositPeriod_;			//get end date for deposit
	return is;
}

ostream& ISAAccount::putAccountDetailsInStream(ostream& os) const {
	//put (unformatted) BankAccount details in stream
	SavingsAccount::putAccountDetailsInStream(os);
	os << maximumYearlyDeposit_ << "\n";
	os << currentYearlyDeposit_ << "\n";
	os << endDepositPeriod_ << "\n";
	return os;
}

const string ISAAccount::prepareFormattedAccountDetails() const
{
	assert(getAccountType(getAccountNumber()[0]) != "UNKNOWN");
	ostringstream os;
	os << SavingsAccount::prepareFormattedAccountDetails();
	os << "\n      MAXIMUM YEARLY DEPOSIT:\234" << setw(10) << maximumYearlyDeposit_;
	os << "\n      CURRENT YEARLY DEPOSIT:\234" << setw(10) << currentYearlyDeposit_;
	os << "\n      END OF DEPOSIT PERIOD :" << setw(10) << endDepositPeriod_.toFormattedString();
	os << "\n      ----------------------------------------";
	return os.str();
}

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------

void ISAAccount::updateCurrentYearlyDeposit(double amount) {
	currentYearlyDeposit_ += amount;
}


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const ISAAccount& aBankAccount) {
	//put (unformatted) BankAccount details in stream
	return aBankAccount.putDataInStream(os);
}
istream& operator>>(istream& is, ISAAccount& aBankAccount) {
	//get BankAccount details from stream
	return aBankAccount.getDataFromStream(is);
}