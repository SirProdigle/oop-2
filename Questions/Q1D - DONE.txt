The UserInterface class is not an abstract class because it's functions contain definitions within the class. Abstract classed only contain declarations of functions which are then given definitions by inheriting classes.

UserInterface should not be an abstract class because there is no need for inheritance within it.