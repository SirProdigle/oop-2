#ifndef SavingsAccountH
#define SavingsAccountH

#include "BankAccount.h"

#include <fstream>
#include <cassert>
using namespace std;

class SavingsAccount : public BankAccount
{
public:
	SavingsAccount();
	virtual ~SavingsAccount();

	double getMinimumBalance() const;

	virtual double maxBorrowable() const;

	virtual const string prepareFormattedAccountDetails() const;

	virtual istream& getAccountDataFromStream(istream& is);
	virtual ostream& putAccountDetailsInStream(ostream& os) const;

private:
	double minimumBalance_;

};

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const SavingsAccount&);	//output operator
istream& operator>>(istream&, SavingsAccount&);	    //input operator

#endif 

