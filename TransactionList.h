//Andreas Sakkas - Liam Warner - Kristian Martin - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Team: (indicate member names, students numbers and courses)

#ifndef TransactionListH
#define TransactionListH

//---------------------------------------------------------------------------
//TransactionList: class declaration
//---------------------------------------------------------------------------

#include "ListT.h"
#include "Transaction.h"
#include <list>
#include <cassert> 	// for assert()
#include <sstream>

class TransactionList {
public:
	void   addNewTransaction(const Transaction&);
    const  Transaction newestTransaction() const;
    const  TransactionList olderTransactions() const;
    void   deleteFirstTransaction();
    void   deleteGivenTransaction(const Transaction&);
	void   deleteTransactionsUpToDate(const Date&);
	int    size() const;
	TransactionList getAllDepositTransactions() const;
	double getTotalTransactions() const;
	TransactionList getTransactionsUpToDate(const Date& d) const;
	TransactionList getMostRecentTransactions(int amount) const;
	TransactionList getTransactionsForAmount(double amount) const;
	TransactionList getTransactionsForTitle(string amount) const;
	TransactionList getTransactionsForDate(Date date) const;
	const string toFormattedString() const;		//return transactionlist as a (formatted) string
	ostream& putDataInStream(ostream& os) const;	//send TransactionList info into an output stream
	istream& getDataFromStream(istream& is);	//receive TransactionList info from an input stream

private:
    std::list<Transaction> listOfTransactions_;	//list of transactions
};

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const TransactionList&);	//insertion operator
istream& operator>>(istream& is, TransactionList& trl); //extraction operator

#endif

