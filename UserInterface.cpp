//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number:
//Team: (indicate member names, students numbers and courses)

#include "UserInterface.h" 

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------

UserInterface::UserInterface(){}

void UserInterface::wait() const
{
	cout << "\n";
	outputLine("Press RETURN to go back to menu...");
	char ch;
	cin.get(ch);
	cin.get(ch);
}
void UserInterface::endProgram() const
{
	cout << "\n";
	outputLine("Press RETURN to end program...");
	char ch;
	cin.get(ch);
	cin.get(ch);
}
int UserInterface::showMainMenuAndGetCommand() const
{
	system("cls");
	cout << "\n\n";
	outputHeader("WELCOME TO THE ATM");
	outputLine(" 0                            Leave ATM ");
	outputLine(" 1                      Enter your card ");
	outputLine("----------------------------------------");
	return (readInCommand());
}

int UserInterface::showAccountMenuAndGetCommand(const string& accNum) const
{
	outputHeader("ACCOUNT " + accNum + " MENU");
	outputLine(" 0                    Back to card menu ");
	outputLine(" 1                      Display balance ");
	outputLine(" 2                Withdraw from account ");
	outputLine(" 3                 Deposit into account ");
	outputLine(" 4                       Show statement ");
	outputLine(" 5                    Show all deposits ");
	outputLine(" 6                  Show mini statement ");
	outputLine(" 7                  Search Transactions ");
	outputLine(" 8    Clear all transactions up to date ");
	outputLine(" 9          Transfer to another account ");
	outputLine("----------------------------------------");
	return (readInCommand());
}

const string UserInterface::readInCardToBeProcessed() const {
	return askForInput("ENTER YOUR CARD NUMBER");
}

void UserInterface::showValidateCardOnScreen(int validCode, const string& cardNumber) const
{
	switch (validCode)
	{
	case VALID_CARD: {
		// Card exists and is accessible (and is not already open: TO BE IMPLEMENTED)
	} break;
	case UNKNOWN_CARD: {
		outputLine("ERROR: CARD " + cardNumber + " DOES NOT EXIST");
	} break;
	case EMPTY_CARD: {
		outputLine("ERROR: CARD " + cardNumber + " DOES NOT LINK TO ANY ACCOUNTS");
	} break;
	}
}

int UserInterface::showCardMenuAndGetCommand(const string& cardNumber) const
{
	outputHeader("CARD " + cardNumber + " MENU");
	outputLine(" 0           Stop banking & remove card ");
	outputLine(" 1            Manage individual account ");
	outputLine(" 2           Show total funds available  // TO BE IMPLEMENTED FOR Task 1c");
	outputLine("----------------------------------------");
	return (readInCommand());
}

void UserInterface::showCardAccounts(const string& cardNumber, const string& cardDetails) const
{
	outputHeader("CARD " + cardNumber + " ACCOUNTS");
	cout << cardDetails;
	outputLine("----------------------------------------\n");
}

const string UserInterface::readInAccountToBeProcessed() const {
	return askForInput("SELECT ACCOUNT TO MANAGE");
}

void UserInterface::showValidateAccountOnScreen(int validCode, const string& accNum) const
{
	switch (validCode)
	{
	case VALID_ACCOUNT:
	{
		outputLine("THE ACCOUNT NUMBER " + accNum + " IS NOW OPEN!");
	} break;
	case INVALID_ACCOUNT_TYPE:
	{
		outputLine("ERROR: ACCOUNT " + accNum + " IS NOT A RECOGNISED TYPE OF ACCOUNT!");
	} break;
	case UNKNOWN_ACCOUNT:
	{
		outputLine("ERROR: ACCOUNT " + accNum + " DOES NOT EXIST!");
	} break;
	case UNACCESSIBLE_ACCOUNT:
	{
		outputLine("ERROR: ACCOUNT " + accNum + " IS NOT ACCESSIBLE WITH THIS CARD!");
	} break;
	}
}

void UserInterface::showCardOnScreen(string card)
{

}

//static 
const string UserInterface::cardFilename(const string& cn) {
	//read in card name & produce cashcard filename
	return FILEPATH + "card_" + cn + ".txt";	//read in card name & produce cashcard filename
}
//static
const string UserInterface::accountFilename(const string& an) {
	return FILEPATH + "account_" + an + ".txt";
}

//input functions

double UserInterface::readInWithdrawalAmount() const {
	//ask for the amount to withdraw
	outputLine("AMOUNT TO WITHDRAW: \234");
	return (readInPositiveAmount());
}
double UserInterface::readInDepositAmount() const {
	//ask for the amount to deposit
	outputLine("AMOUNT TO DEPOSIT: \234");
	return (readInPositiveAmount());
}
double UserInterface::readInTransferAmount() const
{
	outputLine("AMOUNT TO TRANSFER: \234");
	return (readInPositiveAmount());
}
Date UserInterface::readInValidDate(Date& cd) const {
	//ask for a valid date
	int day_, month_, year_;
	outputLine("ENTER DAY: ");
	cin >> day_;
	outputLine("ENTER MONTH: ");
	cin >> month_;
	outputLine("ENTER YEAR: ");
	cin >> year_;
	
	Date d(day_, month_, year_);

	while (!d.isValid(cd)) {
		outputLine("ERROR INVLAID DATE ENTER NEW DATE");
		outputLine("ENTER DAY: ");
		cin >> day_;
		outputLine("ENTER MONTH: ");
		cin >> month_;
		outputLine("ENTER YEAR: ");
		cin >> year_;

		d.setDate(day_, month_, year_);
	}

	return d;
}
bool UserInterface::readInConfirmDelete() const {
	char ch;
	outputLine("DO YOU WISH TO DELETE THE TRANSACTION (Y or N): ");
	cin >> ch;
	if (ch == 'Y' || ch == 'y')
		return true;
	else if (ch == 'N' || ch == 'n')
		return false;

}

int UserInterface::readInNumberOfTransactions() const
{
	cout << "\nInput how many transactions you want to see";
	int amount = readInPositiveNumber();
	return amount;
}

int UserInterface::readInPositiveNumber() const
{
	int posNumber(0);
	while (posNumber <= 0) {
		cin >> posNumber;
		if (posNumber <= 0) {
			cout << "\n Input a positive number";
		}
	}
	return posNumber;

	

}

string UserInterface::readInTitle() const
{
	string x = askForInput("Input Title");
	return x;
}


//output functions

void UserInterface::showProduceBalanceOnScreen(double balance) const
{
	cout << "\n      THE CURRENT BALANCE IS: " << fixed << setprecision(2) << setfill(' ') << "\234" << setw(12) << balance;
}

void UserInterface::showWithdrawalOnScreen(bool trAuthorised, double withdrawnAmount) const
{
	if (trAuthorised)
	{
		cout << "\n      TRANSACTION AUTHORISED. \234" << setw(0) << withdrawnAmount << " WITHDRAWN FROM ACCOUNT";
	}
	else
	{
		outputLine("TRANSACTION IMPOSSIBLE!"); // not enough money
	}
}

void UserInterface::showDepositOnScreen(bool trAuthorised, double depositAmount) const
{
	if (trAuthorised)
	{
		cout << "\n      TRANSACTION AUTHORISED. \234" << setw(0) << depositAmount << " DEPOSITED INTO ACCOUNT";
	}
	else
	{
		outputLine("TRANSACTION IMPOSSIBLE!"); // too much to deposit
	}
}
void UserInterface::showStatementOnScreen(const string& statement) const {
	outputHeader("PREPARING STATEMENT...");
	cout << statement;
	outputLine("----------------------------------------\n");
}

void UserInterface::showMiniStatementOnScreen(bool isEmpty, double total, std::string statement) const
{
	Time now = Time::currentTime();
	Date today = Date::currentDate();
	cout << "\n\tRECENT TRANSACTIONS REQUESTED AT: " << now.toFormattedString() << " ON " << today.toFormattedString();
	if (isEmpty) {
		//No statement so print out empty message and return
		cout << "\nNO TRANSACTIONS IN BANK ACCOUNT";
	}
	else {
		cout << statement;
		cout << "\n\tTotal transaction value in statement:" << total;
	}
	cout << "\n\n\nSuccess -- Press Return to go back to account window";
}



void UserInterface::showAllDepositsOnScreen(bool noTransaction, string& str, double& total) const {
	if (noTransaction==true)
	{
		outputLine("NO TRANSACTIONS IN BANK ACCOUNT");
	}
	else
	{
		cout << str;
		outputLine("TOTAL OF THE TRANSACTIONS IS ");
		cout << total;
	}
}

void UserInterface::showTransactionsUpToDate(bool noTrans, Date& d, string& str, int& n) const {

	if (noTrans)
		outputLine("NO TRANSACTIONS IN BANK ACCOUNT");
	else
	{
		if (n == 0) {
			outputLine("NO TRANSACTION UP TO DATE " + d.toFormattedString());
			return;
		}
		else {
			cout << str;
		}

	}
}

void UserInterface::showDeletionOfTransactionsUpToDate(int n, Date& d, bool deletionConfirmed) const {

	string str(d.toFormattedString());
	outputLine("THE" + n );
	cout << " TRANSACTIONS IN BANK ACCOUNT UP TO DATE " << str << " HAVE BEEN DELETED";
}

void UserInterface::showSearchMenu() const
{
	outputLine("");
	outputLine("1: By Amount");
	outputLine("2: By Title");
	outputLine("3: By Date");
	outputLine("4: Exit");
}

int UserInterface::readInSearchCommand() const
{
	int command = this->readInCommand();
	return command;
}

void UserInterface::showNoTransactionsOnScreen() const
{
	// Write this. error for now LIAM TODO
}

double UserInterface::readInDouble() const
{
	outputLine("");
	outputLine("Input amount to search for");
	double x;
	cin >> x;
	return x;
}

void UserInterface::showMatchingTransactionsOnScreen(double amount, int count, string message) const
{
	outputHeader("SEARCH RESULTS FOR " + to_string(amount) + " : " + to_string(count) + " RESULTS");
	outputLine("");
	outputLine(message);
}

void UserInterface::showMatchingTransactionsOnScreen(string title, int count, string message) const
{
	outputHeader("SEARCH RESULTS FOR TITLE " + title + " : " + to_string(count) + " RESULTS");
	outputLine("");
	outputLine(message);
}

void UserInterface::showMatchingTransactionsOnScreen(Date date, int count, string message) const
{
	outputHeader("SEARCH RESULTS FOR " +  date.toFormattedString() + " : " + to_string(count) + " RESULTS");
	outputLine("");
	outputLine(message);
}

void UserInterface::showFundsAvailableOnScreen(double total, string message) const
{
	outputHeader("TOTAL AMOUNT CAN WITHDRAW: " + to_string(total));
	outputLine("");
	outputLine(message);
}

void UserInterface::showErrorMessage(string message) const
{
	outputLine("");
	outputLine(message);
	outputLine("");
}

void UserInterface::showTransferOnScreen(bool outOk, bool inOk, double transferAmount) const
{
	if (outOk && inOk)
	{
		outputLine("TRANSFER AMMOUNT HAS GONE THROUGH OF " );
		cout << transferAmount;
	}
	else
	{
		outputLine("TRANSFER NOT SUCCESSFUL");
	}
}

//---------------------------------------------------------------------------
// private support member functions
//---------------------------------------------------------------------------

void UserInterface::showByeScreen() const
{
	outputLine("");
	outputHeader("THANK YOU FOR USING THE ATM");
	endProgram();
}

int UserInterface::readInCommand() const
{
	cout << "\n";
	outputLine("ENTER YOUR COMMAND: ");
	int com;
	cin >> com;
	return com;
}

void UserInterface::showErrorInvalidCommand() const
{
	outputLine("INVALID COMMAND CHOICE, TRY AGAIN");
}

double UserInterface::readInPositiveAmount() const
{
	double amount;
	cin >> amount;

	while (amount <= 0.0)
	{
		outputLine("AMOUNT SHOULD BE A POSITIVE AMOUNT, TRY AGAIN: ");
		cin >> amount;
	}

	return amount;
}

void UserInterface::outputHeader(const string& header) const
{
	// calculate lengths so we can centre the header
	const int length = header.size();
	const int borderWidth = 40;
	const int leftSpacePad = 6;
	const int paddingRequired = ((borderWidth - length) / 2) + leftSpacePad;

	outputLine("========================================");
	cout << "\n" << setfill(' ') << setw(paddingRequired) << ' ' << header;
	outputLine("========================================");
}

string UserInterface::askForInput(const string& promptForUser) const
{
	outputLine(promptForUser + ": ");
	string userInput;
	cin >> userInput;
	return userInput;
}

void UserInterface::outputLine(const string& text) const
{
	cout << "\n      " << text;
}