#ifndef ISAAccountH
#define ISAAccountH

#include "SavingsAccount.h"

#include <fstream>
#include <cassert>
using namespace std;

class ISAAccount : public SavingsAccount
{
public:
	ISAAccount();
	virtual ~ISAAccount();

	double getMaximumYearlyDeposit() const;
	double getCurrentYearlyDeposit() const;
	Date getEndDepositPeriod() const;

	virtual void recordDeposit(double);

	virtual bool canDeposit(double) const;

	virtual const string prepareFormattedAccountDetails() const;

	virtual istream& getAccountDataFromStream(istream& is);
	virtual ostream& putAccountDetailsInStream(ostream& os) const;


private:
	double maximumYearlyDeposit_;
	double currentYearlyDeposit_;
	Date endDepositPeriod_;

	void updateCurrentYearlyDeposit(double);

};


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const ISAAccount&);	//output operator
istream& operator>>(istream&, ISAAccount&);	    //input operator



#endif 

